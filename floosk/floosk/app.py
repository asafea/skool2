from flask import Flask, render_template, url_for, request,redirect
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///test.db"
db = SQLAlchemy(app)

winningTeam = ""
checkingString = ""
flag = 0

@app.route('/', methods=['GET'])
def index():
    global winningTeam
    global checkingString

    return render_template('index.html',name=winningTeam,checkingString=checkingString)


@app.route('/submitGroupName/<name>')
def win(name):
    global winningTeam
    global checkingString

    checkingString += "1"
    winningTeam = name
    print("{} won!!!".format(name))
    return redirect('/')

@app.route('/submitPassword/<passw>')
def tryPass(passw):
    global flag

    if(passw == "saucySauce"):
        flag = 1
    
    return redirect('/finalStep')

@app.route('/clue')
def clue():
    return render_template('clue.html')

@app.route('/finalStep')
def final():
    global flag

    return render_template('final.html', flag=flag)

if __name__ == '__main__':
    app.run(port=80,host='10.140.228.146')